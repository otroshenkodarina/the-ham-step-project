$(document).ready(function () {
    $('.grid').masonry({
        columnWidth: 370,
        itemSelector: '.grid-item',
        percentPosition: true,
        gutter: 17
    });
});

