const serviceNavItem = document.querySelectorAll('.service-nav-item')


serviceNavItem.forEach(function (item) {
    item.addEventListener('click', function (e) {
        const name = this.dataset.name
        const content = document.querySelector(`.service-nav-desc[id=${name}]`)

        const activeTitle = document.querySelector('.service-nav-item.active')
        const activeContent = document.querySelector('.service-nav-desc.active-card')

        activeTitle.classList.remove('active')
        activeContent.classList.remove('active-card')

        this.classList.add('active')
        content.classList.add('active-card')
    })
})

const loadMoreButton = document.querySelector('.gallery-btn-block')

loadMoreButton.addEventListener('click', (e) => {

    loadMoreButton.classList.add('none-button')
    const loading = document.querySelector('.bubblingG')
    loading.classList.add('active-bubbles')

    setTimeout(function () {
        loading.classList.remove('active-bubbles')
        loadMoreButton.classList.remove('none-button')

        const imagesBlock = document.querySelectorAll('.gallery-images-block2')

        imagesBlock.forEach((item) => {
            item.classList.add('active-item')
        })
        // imagesBlock.classList.add('active-gallery')

        // const image = document.querySelectorAll('.gallery-item')
        // image.forEach((item) => {
        //     item.classList.add('active-item')
        // })
    }, 2000)

    loadMoreButton.addEventListener('click', () => {

        loading.classList.add('active-bubbles')

        setTimeout(function () {
            loading.remove()

            const imagesBlock = document.querySelectorAll('.gallery-images-block3')

            imagesBlock.forEach((item) => {
                item.classList.add('active-item')
            })

            const galleryBlock = document.querySelector('.gallery-block')
            galleryBlock.classList.add('active-gallery-block')
            // imagesBlock.classList.add('active-gallery')
            //
            // const image = document.querySelectorAll('.gallery-item')
            // image.forEach((item) => {
            //     item.classList.add('active-item')
            // })

            loadMoreButton.remove()
        }, 2000)
    })
})


const galleryNavItem = document.querySelectorAll('.gallery-nav-item')

galleryNavItem.forEach(function (item) {
    item.addEventListener('click', function (e) {

        const imagesBlock = document.querySelector('.gallery-images-block2')
        imagesBlock.classList.add('active-gallery')

        const imagesBlock3 = document.querySelector('.gallery-images-block3')
        imagesBlock3.classList.add('active-gallery')

        const name = this.id
        const content = document.querySelectorAll(`.gallery-item[data-name=${name}]`)


        const activeTitle = document.querySelector('.gallery-nav-item.active-nav-item')
        const activeContent = document.querySelectorAll('.gallery-item.active-item')

        activeTitle.classList.remove('active-nav-item')
        activeContent.forEach(function (item) {
            item.classList.remove('active-item')
        })

        this.classList.add('active-nav-item')

        content.forEach(function (item) {
            item.classList.add('active-item')
        })

        const galleryBlock = document.querySelector('.gallery-block')
        galleryBlock.classList.add('active-gallery-block')

        loadMoreButton.remove()
    })
})

const allImages = document.getElementById('all')

allImages.addEventListener('click', () => {
    allImages.classList.add('active-item')
    const imagesBlock = document.querySelectorAll('.gallery-item')
    imagesBlock.forEach((item) => {
        item.classList.add('active-item')
    })
    const galleryBlock = document.querySelector('.gallery-block')
    galleryBlock.classList.add('active-gallery-block')
})

function toggleArrowCarousel(index, array){
    const name = array[index].id
    const content = document.querySelector(`.reviews-text[data-name=${name}]`)
    const personName = document.querySelector(`.reviews-person-name[data-name=${name}]`)
    const position = document.querySelector(`.reviews-person-position[data-name=${name}]`)
    const photo = document.querySelector(`.reviews-person-photo[data-name=${name}]`)

    const activePersonImage = document.querySelector('.reviews-person-img.active-person-img')
    const activeContent = document.querySelector('.reviews-text.active-text')
    const activePersonName = document.querySelector('.reviews-person-name.active-person-name')
    const activePosition = document.querySelector('.reviews-person-position.active-person-position')
    const activePhoto = document.querySelector('.reviews-person-photo.active-person-photo')

    activePersonImage.classList.remove('active-person-img')
    activeContent.classList.remove('active-text')
    activePersonName.classList.remove('active-person-name')
    activePosition.classList.remove('active-person-position')
    activePhoto.classList.remove('active-person-photo')

    content.classList.add('active-text')
    personName.classList.add('active-person-name')
    position.classList.add('active-person-position')
    photo.classList.add('active-person-photo')

    array[index].classList.add('active-person-img')
}

function swipeRight() {
    const rightButton = document.getElementById('right')

    rightButton.addEventListener('click', function () {
        const personImage = document.querySelectorAll('.reviews-person-img')
        const personImageArray = [...personImage]
        let currentIndex = 0;

        personImageArray.forEach((item, key) => {
            if (item.classList.value.includes('active-person-img')) currentIndex = key;
        })

        if (currentIndex === personImageArray.length - 1) currentIndex = 0
        else currentIndex = currentIndex + 1

        toggleArrowCarousel(currentIndex, personImageArray)
    })
}

function swipeLeft() {
    const leftButton = document.getElementById('left')

    leftButton.addEventListener('click', function () {
        const personImage = document.querySelectorAll('.reviews-person-img')
        const personImageArray = [...personImage]
        let currentIndex = 0;

        personImageArray.forEach((item, key) => {
            if (item.classList.value.includes('active-person-img')) currentIndex = key;
        })

        if (currentIndex === 0) currentIndex = personImageArray.length - 1
        else currentIndex = currentIndex - 1

        toggleArrowCarousel(currentIndex, personImageArray)
    })


}

swipeRight()
swipeLeft()

const personImage = document.querySelectorAll('.reviews-person-img')

personImage.forEach(function (item) {
    item.addEventListener('click', function (e) {
        const name = this.id
        const content = document.querySelector(`.reviews-text[data-name=${name}]`)
        const personName = document.querySelector(`.reviews-person-name[data-name=${name}]`)
        const position = document.querySelector(`.reviews-person-position[data-name=${name}]`)
        const photo = document.querySelector(`.reviews-person-photo[data-name=${name}]`)

        const activePersonImage = document.querySelector('.reviews-person-img.active-person-img')
        const activeContent = document.querySelector('.reviews-text.active-text')
        const activePersonName = document.querySelector('.reviews-person-name.active-person-name')
        const activePosition = document.querySelector('.reviews-person-position.active-person-position')
        const activePhoto = document.querySelector('.reviews-person-photo.active-person-photo')

        activePersonImage.classList.remove('active-person-img')
        activeContent.classList.remove('active-text')
        activePersonName.classList.remove('active-person-name')
        activePosition.classList.remove('active-person-position')
        activePhoto.classList.remove('active-person-photo')

        this.classList.add('active-person-img')
        content.classList.add('active-text')
        personName.classList.add('active-person-name')
        position.classList.add('active-person-position')
        photo.classList.add('active-person-photo')
    })
})

const loadMoreButton2 = document.getElementById('button')

loadMoreButton2.addEventListener('click', function (){
    loadMoreButton2.classList.add('none-button')
    const loading = document.querySelector('.bubblingG2')
    loading.classList.add('active-bubbles')

    setTimeout(function () {
        loading.classList.remove('active-bubbles')

        const imagesBlock = document.querySelectorAll('.none-grid')
        const imagesBlockArray = [...imagesBlock]

        imagesBlockArray.forEach(item => {
            item.classList.remove('none-grid')
        })

        $('.grid').masonry({
            columnWidth: 370,
            itemSelector: '.grid-item',
            percentPosition: true,
            gutter: 17
        });

    }, 2000)
})













